import axios from "axios";
// import key from "../env";

const serviceInstance = axios.create({
  baseURL: "https://api.themoviedb.org/3",
  Headers: { Accept: "application/json" },
  timeout: 10000
});

export default {
  getPopularMovies() {
    return serviceInstance.get(`/movie/popular?api_key=${process.env.VUE_APP_MY_API_KEY}&language=en-US`);
  },
  getTrendingMovies() {
    return serviceInstance.get(`/trending/movie/week?api_key=${process.env.VUE_APP_MY_API_KEY}&language=en-US`);
  },
  getTopMovies() {
    return serviceInstance.get(`/movie/top_rated?api_key=${process.env.VUE_APP_MY_API_KEY}&language=en-US`);
  },
};
